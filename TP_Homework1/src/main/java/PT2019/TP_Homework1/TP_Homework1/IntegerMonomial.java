package PT2019.TP_Homework1.TP_Homework1;

public class IntegerMonomial extends Monomial {

	private Integer coefficient;

	@Override
	public Number getCoefficient() {
		return coefficient;
	}

	@Override
	public void setCoefficient(Number coefficient) {
		this.coefficient = (Integer) coefficient; // downcasting
	}

}
