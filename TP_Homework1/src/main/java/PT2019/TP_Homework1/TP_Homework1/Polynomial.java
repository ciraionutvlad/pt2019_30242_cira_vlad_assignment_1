package PT2019.TP_Homework1.TP_Homework1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Polynomial {

	private List<IntegerMonomial> monomialsList;

	public List<IntegerMonomial> getMonomialsList() {
		return monomialsList;
	}

	public void setMonomialsList(List<IntegerMonomial> monomialsList) {
		this.monomialsList = monomialsList;
	}

	public void createPolynomial(int degree, ArrayList<Integer> coefficientsList) {
		if (degree < 0) {
			System.out.println("Grad negativ !");
		} else {
			monomialsList = new ArrayList<IntegerMonomial>();
			for (Integer i : coefficientsList) {
				IntegerMonomial monomial = new IntegerMonomial();
				monomial.setDegree(degree);
				monomial.setCoefficient(i);
				monomialsList.add(monomial);
				degree--;
			}
		}
	}

	public Polynomial addPolynomial(Polynomial polynomial) {
		Polynomial sumPolynomial = new Polynomial();
		List<IntegerMonomial> sumMonomialsList = new ArrayList<IntegerMonomial>();

		int checkDegree;
		int firstPolynomialDegree = monomialsList.size();
		int secondPolynomialDegree = polynomial.getMonomialsList().size();

		if (firstPolynomialDegree < secondPolynomialDegree) {
			checkDegree = 1;
		} else {
			checkDegree = 2;
		}

		switch (checkDegree) {
		case 1:
			for (int i = 0; i < firstPolynomialDegree; i++) {
				IntegerMonomial monomialFirstPolynomial = getMonomialsList().get(i);
				IntegerMonomial monomialSecondPolynomial = polynomial.getMonomialsList().get(i);
				int sumCoefficient = monomialFirstPolynomial.getCoefficient().intValue()
						+ monomialSecondPolynomial.getCoefficient().intValue();
				IntegerMonomial monomialSum = new IntegerMonomial();
				monomialSum.setCoefficient(sumCoefficient);
				monomialSum.setDegree(i);
				sumMonomialsList.add(monomialSum);
			}
			for (int i = firstPolynomialDegree; i < secondPolynomialDegree; i++) {
				IntegerMonomial monomialSecondPolynomial = polynomial.getMonomialsList().get(i);
				int sumCoefficient = monomialSecondPolynomial.getCoefficient().intValue();
				IntegerMonomial monomialSum = new IntegerMonomial();
				monomialSum.setCoefficient(sumCoefficient);
				monomialSum.setDegree(i);
				sumMonomialsList.add(monomialSum);
			}
			break;
		case 2:
			for (int i = 0; i < secondPolynomialDegree; i++) {
				IntegerMonomial monomialFirstPolynomial = getMonomialsList().get(i);
				IntegerMonomial monomialSecondPolynomial = polynomial.getMonomialsList().get(i);
				int sumCoefficient = monomialFirstPolynomial.getCoefficient().intValue()
						+ monomialSecondPolynomial.getCoefficient().intValue();
				IntegerMonomial monomialSum = new IntegerMonomial();
				monomialSum.setCoefficient(sumCoefficient);
				monomialSum.setDegree(i);
				sumMonomialsList.add(monomialSum);
			}
			for (int i = secondPolynomialDegree; i < firstPolynomialDegree; i++) {
				IntegerMonomial monomialFirstPolynomial = getMonomialsList().get(i);
				int sumCoefficient = monomialFirstPolynomial.getCoefficient().intValue();
				IntegerMonomial monomialSum = new IntegerMonomial();
				monomialSum.setCoefficient(sumCoefficient);
				monomialSum.setDegree(i);
				sumMonomialsList.add(monomialSum);
			}
		}

		sumPolynomial.setMonomialsList(sumMonomialsList);

		return sumPolynomial;
	}

	public Polynomial substractPolynomial(Polynomial polynomial) {
		Polynomial differencePolynomial = new Polynomial();
		List<IntegerMonomial> differenceMonomialsList = new ArrayList<IntegerMonomial>();

		int checkDegree;
		int firstPolynomialDegree = monomialsList.size();
		int secondPolynomialDegree = polynomial.getMonomialsList().size();

		if (firstPolynomialDegree < secondPolynomialDegree) {
			checkDegree = 1;
		} else {
			checkDegree = 2;
		}

		switch (checkDegree) {
		case 1:
			for (int i = 0; i < firstPolynomialDegree; i++) {
				IntegerMonomial monomialFirstPolynomial = getMonomialsList().get(i);
				IntegerMonomial monomialSecondPolynomial = polynomial.getMonomialsList().get(i);
				int differenceCoefficient = monomialFirstPolynomial.getCoefficient().intValue()
						- monomialSecondPolynomial.getCoefficient().intValue();
				IntegerMonomial monomialDifference = new IntegerMonomial();

				monomialDifference.setCoefficient(differenceCoefficient);
				monomialDifference.setDegree(i);

				System.out.println(
						"test1: " + monomialDifference.getDegree() + ", " + monomialDifference.getCoefficient());

				differenceMonomialsList.add(monomialDifference);
			}
			for (int i = firstPolynomialDegree; i < secondPolynomialDegree; i++) {
				IntegerMonomial monomialSecondPolynomial = polynomial.getMonomialsList().get(i);
				int differenceCoefficient = ((-1) * monomialSecondPolynomial.getCoefficient().intValue());
				IntegerMonomial monomialDifference = new IntegerMonomial();

				monomialDifference.setCoefficient(differenceCoefficient);
				monomialDifference.setDegree(i);

				System.out.println(
						"test1: " + monomialDifference.getDegree() + ", " + monomialDifference.getCoefficient());

				differenceMonomialsList.add(monomialDifference);
			}
			break;
		case 2:
			for (int i = 0; i < secondPolynomialDegree; i++) {
				IntegerMonomial monomialFirstPolynomial = getMonomialsList().get(i);
				IntegerMonomial monomialSecondPolynomial = polynomial.getMonomialsList().get(i);
				int differenceCoefficient = monomialFirstPolynomial.getCoefficient().intValue()
						- monomialSecondPolynomial.getCoefficient().intValue();
				IntegerMonomial monomialDifference = new IntegerMonomial();

				monomialDifference.setCoefficient(differenceCoefficient);
				monomialDifference.setDegree(i);

				System.out.println(
						"test2: " + monomialDifference.getDegree() + ", " + monomialDifference.getCoefficient());

				differenceMonomialsList.add(monomialDifference);
			}
			for (int i = secondPolynomialDegree; i < firstPolynomialDegree; i++) {
				IntegerMonomial monomialFirstPolynomial = getMonomialsList().get(i);
				int differenceCoefficient = monomialFirstPolynomial.getCoefficient().intValue();
				IntegerMonomial monomialDifference = new IntegerMonomial();

				monomialDifference.setCoefficient(differenceCoefficient);
				monomialDifference.setDegree(i);

				System.out.println(
						"test2: " + monomialDifference.getDegree() + ", " + monomialDifference.getCoefficient());

				differenceMonomialsList.add(monomialDifference);
			}
			break;
		default: {
			break;
		}
		}

		differencePolynomial.setMonomialsList(differenceMonomialsList);

		return differencePolynomial;
	}

	public Polynomial multiplyPolynomial(Polynomial polynomial) {
		Polynomial productPolynomial = new Polynomial();
		HashMap<Integer, IntegerMonomial> map = new HashMap<Integer, IntegerMonomial>();

		int firstPolynomialDegree = monomialsList.size();
		int secondPolynomialDegree = polynomial.getMonomialsList().size();

		for (int i = 0; i < firstPolynomialDegree; i++) {
			for (int j = 0; j < secondPolynomialDegree; j++) {
				IntegerMonomial monomialFirstPolynomial = getMonomialsList().get(i);
				IntegerMonomial monomialSecondPolynomial = polynomial.getMonomialsList().get(j);
				int productCoefficient = monomialFirstPolynomial.getCoefficient().intValue()
						* monomialSecondPolynomial.getCoefficient().intValue();
				IntegerMonomial monomialProduct = new IntegerMonomial();

				if (map.containsKey(i + j)) {
					IntegerMonomial m = map.get(i + j);
					monomialProduct.setCoefficient(productCoefficient + m.getCoefficient().intValue());
				} else {
					monomialProduct.setCoefficient(productCoefficient);
				}

				monomialProduct.setDegree(i + j);
				map.put(i + j, monomialProduct);
			}
		}

		List<IntegerMonomial> productMonomialsList = new ArrayList<IntegerMonomial>();
		for (IntegerMonomial m : map.values()) {
			productMonomialsList.add(m);
			System.out.println("test: " + m.getDegree() + ", " + m.getCoefficient());
		}

		productPolynomial.setMonomialsList(productMonomialsList);

		return productPolynomial;
	}

	@Override
	public String toString() {
		int degree = monomialsList.size() - 1;
		if (degree == -1)
			return "0";
		else if (degree == 0)
			return "" + monomialsList.get(0).getCoefficient();
		else if (degree == 1)
			return monomialsList.get(1).getCoefficient() + "x + " + monomialsList.get(0).getCoefficient();
		String s;
		if (monomialsList.get(degree).getCoefficient().intValue() == 1) {
			s = "x^" + degree;
		} else {
			s = monomialsList.get(degree).getCoefficient() + "x^" + degree;
		}

		for (int i = degree - 1; i >= 0; i--) {
			if (monomialsList.get(i).getCoefficient().intValue() == 0)
				continue;
			else if (monomialsList.get(i).getCoefficient().intValue() > 0) {
				if (monomialsList.get(i).getCoefficient().intValue() == 1) {
					s = s + "+";
				} else
					s = s + " + " + (monomialsList.get(i).getCoefficient());
			} else if (monomialsList.get(i).getCoefficient().intValue() < 0)
				s = s + " - " + (-monomialsList.get(i).getCoefficient().intValue());

			if (i == 1)
				s = s + "x";
			else if (i > 1)
				s = s + "x^" + i;
		}
		return s;
	}
}
