package PT2019.TP_Homework1.TP_Homework1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyGUI extends JFrame {

	private JPanel panel;
	private JLabel firstPolynomialDegreeLabel;
	private JLabel firstPolynomialCoefficientsLabel;
	private JLabel firstPolynomialLabel;
	private JLabel secondPolynomialDegreeLabel;
	private JLabel secondPolynomialCoefficientsLabel;
	private JLabel secondPolynomialLabel;
	private JTextField firstPolynomialDegreeTextField;
	private JTextField firstPolynomialCoefficientsTextField;
	private JTextField firstPolynomialForm;
	private JTextField secondPolynomialDegreeTextField;
	private JTextField secondPolynomialCoefficientsTextField;
	private JTextField secondPolynomialForm;
	private JTextField sumPolynomials;
	private JTextField differencePolynomials;
	private JTextField productPolynomials;
	private JTextField quotientPolynomials;
	private JTextField remainderPolynomials;
	private JTextField derivativedFirstPolynomial;
	private JTextField derivativedSecondPolynomial;
	private JTextField integratedFirstPolynomial;
	private JTextField integratedSecondPolynomial;
	private JButton createFirstPolynomialButton;
	private JButton createSecondPolynomialButton;
	private JButton addPolynomials;
	private JButton substractPolynomials;
	private JButton multiplyPolynomials;
	private JButton dividePolynomials;
	private JButton derivativeFirstPolynomial;
	private JButton derivativeSecondPolynomial;
	private JButton integrateFirstPolynomial;
	private JButton integrateSecondPolynomial;

	private Polynomial firstPolynomial;
	private Polynomial secondPolynomial;

	public MyGUI() {

		// Setting the width and the height of the frame
		setSize(560, 580);

		// Closing the frame by clicking the X button
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Initialise the GUI components
		initialiseGUIComponents();

		// Displaying the frame
		setVisible(true);
	}

	public static void main(String[] args) {
		// Create the GUI
		MyGUI myGUI = new MyGUI();
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		
		ArrayList<Integer> p1Polynomial = new ArrayList<Integer>();
		ArrayList<Integer> p2Polynomial = new ArrayList<Integer>();

		p1Polynomial.add(1);
		p1Polynomial.add(1);
		p1Polynomial.add(2);
		p1Polynomial.add(10);
		// p1Polynomial.add(999);

		p2Polynomial.add(4);
		p2Polynomial.add(6);
		p2Polynomial.add(8);
		// p2Polynomial.add(10);

		p1.createPolynomial(3, p1Polynomial);
		p2.createPolynomial(2, p2Polynomial);	

	}

	private void initialiseGUIComponents() {
		// Create the panel
		panel = new JPanel();

		// Setting the layout null
		panel.setLayout(null);

		// Label for First Polynomial Degree
		firstPolynomialDegreeLabel = new JLabel("First Polynomial Degree :");
		firstPolynomialDegreeLabel.setBounds(10, 20, 200, 25);
		panel.add(firstPolynomialDegreeLabel);
		// TextField for First Polynomial Degree
		firstPolynomialDegreeTextField = new JTextField();
		firstPolynomialDegreeTextField.setBounds(210, 20, 20, 25);
		panel.add(firstPolynomialDegreeTextField);

		// Label for First Polynomial Coefficients
		firstPolynomialCoefficientsLabel = new JLabel("First Polynomial Coefficients :");
		firstPolynomialCoefficientsLabel.setBounds(10, 50, 200, 25);
		panel.add(firstPolynomialCoefficientsLabel);
		// TextField for First Polynomial Coefficients
		firstPolynomialCoefficientsTextField = new JTextField();
		firstPolynomialCoefficientsTextField.setBounds(210, 50, 165, 25);
		panel.add(firstPolynomialCoefficientsTextField);

		// Button for Create First Polynomial
		createFirstPolynomialButton = new JButton("Create First Polynomial");
		createFirstPolynomialButton.setBounds(10, 80, 190, 25);
		panel.add(createFirstPolynomialButton);
		// Label for First Polynomial
		firstPolynomialLabel = new JLabel("First Polynomial :");
		firstPolynomialLabel.setBounds(10, 110, 200, 25);
		panel.add(firstPolynomialLabel);
		// TextField for First Polynomial
		firstPolynomialForm = new JTextField();
		firstPolynomialForm.setBounds(130, 110, 245, 25);
		firstPolynomialForm.setEnabled(false);
		panel.add(firstPolynomialForm);

		// Label for Second Polynomial Degree
		secondPolynomialDegreeLabel = new JLabel("Second Polynomial Degree :");
		secondPolynomialDegreeLabel.setBounds(10, 150, 200, 25);
		panel.add(secondPolynomialDegreeLabel);
		// TextField for Second Polynomial Degree
		secondPolynomialDegreeTextField = new JTextField();
		secondPolynomialDegreeTextField.setBounds(210, 150, 20, 25);
		panel.add(secondPolynomialDegreeTextField);

		// Label for Second Polynomial Coefficients
		secondPolynomialCoefficientsLabel = new JLabel("Second Polynomial Coefficients :");
		secondPolynomialCoefficientsLabel.setBounds(10, 180, 200, 25);
		panel.add(secondPolynomialCoefficientsLabel);
		// TextField for Second Polynomial Coefficients
		secondPolynomialCoefficientsTextField = new JTextField();
		secondPolynomialCoefficientsTextField.setBounds(210, 180, 165, 25);
		panel.add(secondPolynomialCoefficientsTextField);

		// Button for Create Second Polynomial
		createSecondPolynomialButton = new JButton("Create Second Polynomial");
		createSecondPolynomialButton.setBounds(10, 210, 190, 25);
		panel.add(createSecondPolynomialButton);
		// Label for Second Polynomial
		secondPolynomialLabel = new JLabel("Second Polynomial :");
		secondPolynomialLabel.setBounds(10, 240, 200, 25);
		panel.add(secondPolynomialLabel);
		// TextField for Second Polynomial
		secondPolynomialForm = new JTextField();
		secondPolynomialForm.setBounds(130, 240, 245, 25);
		secondPolynomialForm.setEnabled(false);
		panel.add(secondPolynomialForm);

		// Button for Addition
		addPolynomials = new JButton("Addition");
		addPolynomials.setBounds(10, 290, 230, 25);
		panel.add(addPolynomials);
		// TextField for sum
		sumPolynomials = new JTextField();
		sumPolynomials.setBounds(250, 290, 285, 25);
		sumPolynomials.setEnabled(false);
		panel.add(sumPolynomials);

		// Button for Subtraction
		substractPolynomials = new JButton("Subtraction");
		substractPolynomials.setBounds(10, 320, 230, 25);
		panel.add(substractPolynomials);
		// TextField for difference
		differencePolynomials = new JTextField();
		differencePolynomials.setBounds(250, 320, 285, 25);
		differencePolynomials.setEnabled(false);
		panel.add(differencePolynomials);

		// Button for Multiplication
		multiplyPolynomials = new JButton("Multiplication");
		multiplyPolynomials.setBounds(10, 350, 230, 25);
		panel.add(multiplyPolynomials);
		// TextField for product
		productPolynomials = new JTextField();
		productPolynomials.setBounds(250, 350, 285, 25);
		productPolynomials.setEnabled(false);
		panel.add(productPolynomials);

		// Button for Division
		dividePolynomials = new JButton("Division");
		dividePolynomials.setBounds(10, 380, 230, 25);
		panel.add(dividePolynomials);
		// TextField for quotient
		quotientPolynomials = new JTextField();
		quotientPolynomials.setBounds(250, 380, 195, 25);
		quotientPolynomials.setEnabled(false);
		panel.add(quotientPolynomials);
		// TextField for remainder
		remainderPolynomials = new JTextField();
		remainderPolynomials.setBounds(450, 380, 85, 25);
		remainderPolynomials.setEnabled(false);
		panel.add(remainderPolynomials);

		// Button for Differentiation First Polynomial
		derivativeFirstPolynomial = new JButton("Differentiation First Polynomial");
		derivativeFirstPolynomial.setBounds(10, 410, 230, 25);
		panel.add(derivativeFirstPolynomial);
		// TextField for derivative
		derivativedFirstPolynomial = new JTextField();
		derivativedFirstPolynomial.setBounds(250, 410, 285, 25);
		derivativedFirstPolynomial.setEnabled(false);
		panel.add(derivativedFirstPolynomial);

		// Button for Differentiation Second Polynomial
		derivativeSecondPolynomial = new JButton("Differentiation Second Polynomial");
		derivativeSecondPolynomial.setBounds(10, 440, 230, 25);
		panel.add(derivativeSecondPolynomial);
		// TextField for derivative
		derivativedSecondPolynomial = new JTextField();
		derivativedSecondPolynomial.setBounds(250, 440, 285, 25);
		derivativedSecondPolynomial.setEnabled(false);
		panel.add(derivativedSecondPolynomial);

		// Button for Integration First Polynomial
		integrateFirstPolynomial = new JButton("Integration First Polynomial");
		integrateFirstPolynomial.setBounds(10, 470, 230, 25);
		panel.add(integrateFirstPolynomial);
		// TextField for integrate
		integratedFirstPolynomial = new JTextField();
		integratedFirstPolynomial.setBounds(250, 470, 285, 25);
		integratedFirstPolynomial.setEnabled(false);
		panel.add(integratedFirstPolynomial);

		// Button for Integration Second Polynomial
		integrateSecondPolynomial = new JButton("Integration Second Polynomial");
		integrateSecondPolynomial.setBounds(10, 500, 230, 25);
		panel.add(integrateSecondPolynomial);
		// TextField for integrate
		integratedSecondPolynomial = new JTextField();
		integratedSecondPolynomial.setBounds(250, 500, 285, 25);
		integratedSecondPolynomial.setEnabled(false);
		panel.add(integratedSecondPolynomial);

		// If the button Create First Polynomial is pressed
		createFirstPolynomialButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String degree = firstPolynomialDegreeTextField.getText();
				String coefficients = firstPolynomialCoefficientsTextField.getText();

				String[] splitedCoefficients = coefficients.split("\\s+");
				Integer[] array = new Integer[splitedCoefficients.length];
				int i = 0;
				for (String str : splitedCoefficients) {
					array[i] = Integer.parseInt(str.trim());
					i++;
				}
				List<Integer> arrayList = Arrays.asList(array);
				firstPolynomial = new Polynomial();
				firstPolynomial.createPolynomial(Integer.parseInt(degree), new ArrayList<Integer>(arrayList));
				firstPolynomialForm.setText(firstPolynomial.toString());
			}
		});

		// If the button Create Second Polynomial is pressed
		createSecondPolynomialButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String degree = secondPolynomialDegreeTextField.getText();
				String coefficients = secondPolynomialCoefficientsTextField.getText();

				String[] splitedCoefficients = coefficients.split("\\s+");
				Integer[] array = new Integer[splitedCoefficients.length];
				int i = 0;
				for (String str : splitedCoefficients) {
					array[i] = Integer.parseInt(str.trim());
					i++;
				}
				List<Integer> arrayList = Arrays.asList(array);
				secondPolynomial = new Polynomial();
				secondPolynomial.createPolynomial(Integer.parseInt(degree), new ArrayList<Integer>(arrayList));
				secondPolynomialForm.setText(secondPolynomial.toString());
			}
		});

		// If the button Addition is pressed
		addPolynomials.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polynomial p = firstPolynomial.addPolynomial(secondPolynomial);
				
				sumPolynomials.setText(p.toString());

			}
		});

		// If the button Subtraction is pressed
		substractPolynomials.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polynomial p = firstPolynomial.substractPolynomial(secondPolynomial);
				
				differencePolynomials.setText(p.toString());
			}
		});

		// If the button Multiplication is pressed
		multiplyPolynomials.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polynomial p = firstPolynomial.multiplyPolynomial(secondPolynomial);
				
				productPolynomials.setText(p.toString());
			}
		});

		// If the button Division is pressed
		dividePolynomials.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String degree = firstPolynomialDegreeTextField.getText();
				String coefficients = firstPolynomialCoefficientsTextField.getText();
				quotientPolynomials.setText(degree + coefficients);
				remainderPolynomials.setText(degree + coefficients);
			}
		});

		// If the button Differentiation First Polynomial is pressed
		derivativeFirstPolynomial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String degree = firstPolynomialDegreeTextField.getText();
				String coefficients = firstPolynomialCoefficientsTextField.getText();
				derivativedFirstPolynomial.setText(degree + coefficients);
			}
		});

		// If the button Differentiation Second Polynomial is pressed
		derivativeSecondPolynomial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String degree = firstPolynomialDegreeTextField.getText();
				String coefficients = firstPolynomialCoefficientsTextField.getText();
				derivativedSecondPolynomial.setText(degree + coefficients);
			}
		});

		// If the button Integration First Polynomial is pressed
		integrateFirstPolynomial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String degree = firstPolynomialDegreeTextField.getText();
				String coefficients = firstPolynomialCoefficientsTextField.getText();
				integratedFirstPolynomial.setText(degree + coefficients);
			}
		});

		// If the button Integration Second Polynomial is pressed
		integrateSecondPolynomial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String degree = firstPolynomialDegreeTextField.getText();
				String coefficients = firstPolynomialCoefficientsTextField.getText();
				integratedSecondPolynomial.setText(degree + coefficients);
			}
		});

		// Adding the panel to frame
		add(panel);
	}

}
