package PT2019.TP_Homework1.TP_Homework1;

public class Monomial {

	private int degree;
	private Number coefficient;

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public Number getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(Number coefficient) {
		this.coefficient = coefficient;
	}

}
