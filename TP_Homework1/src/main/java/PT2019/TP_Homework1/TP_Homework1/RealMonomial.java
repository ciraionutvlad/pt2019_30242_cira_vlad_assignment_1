package PT2019.TP_Homework1.TP_Homework1;

public class RealMonomial extends Monomial {

	private Float coefficient;

	@Override
	public Number getCoefficient() {
		return coefficient;
	}

	@Override
	public void setCoefficient(Number coefficient) {
		this.coefficient = (Float) coefficient; // downcasting
	}

}
