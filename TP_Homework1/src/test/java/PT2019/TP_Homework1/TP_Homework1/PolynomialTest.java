package PT2019.TP_Homework1.TP_Homework1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;

import org.junit.Test;

public class PolynomialTest {

	@Test
	public void testCreatePolynomial_negativeDegree() {
		Polynomial polynomial = new Polynomial();
		polynomial.createPolynomial(-2, new ArrayList<Integer>());
		assertNull(polynomial.getMonomialsList());
	}

	@Test
	public void testCreatePolynomial_Successfuly() {
		Polynomial polynomial = new Polynomial();
		ArrayList<Integer> coefficients = new ArrayList<Integer>();
		coefficients.add(4);
		coefficients.add(3);
		coefficients.add(2);
		polynomial.createPolynomial(2, coefficients);
		assertNotNull(polynomial.getMonomialsList());
	}

	@Test
	public void testAddPolynomial_sameDegree() {
		ArrayList<Integer> firstPolynomialCoeffList = new ArrayList<Integer>();
		firstPolynomialCoeffList.add(4);
		firstPolynomialCoeffList.add(3);
		firstPolynomialCoeffList.add(2);
		Polynomial firstPolynomial = new Polynomial();
		firstPolynomial.createPolynomial(2, firstPolynomialCoeffList);

		ArrayList<Integer> secondPolynomialCoeffList = new ArrayList<Integer>();
		secondPolynomialCoeffList.add(2);
		secondPolynomialCoeffList.add(4);
		secondPolynomialCoeffList.add(3);
		Polynomial secondPolynomial = new Polynomial();
		secondPolynomial.createPolynomial(2, secondPolynomialCoeffList);

		Polynomial resultPolynomial = firstPolynomial.addPolynomial(secondPolynomial);

		assertEquals("5x^2 + 7x + 6", resultPolynomial.toString());
	}

	@Test
	public void testAddPolynomial_firstPolynomialHigherDegree() {
		ArrayList<Integer> firstPolynomialCoeffList = new ArrayList<Integer>();
		firstPolynomialCoeffList.add(4);
		firstPolynomialCoeffList.add(3);
		firstPolynomialCoeffList.add(2);
		firstPolynomialCoeffList.add(4);
		Polynomial firstPolynomial = new Polynomial();
		firstPolynomial.createPolynomial(3, firstPolynomialCoeffList);

		ArrayList<Integer> secondPolynomialCoeffList = new ArrayList<Integer>();
		secondPolynomialCoeffList.add(2);
		secondPolynomialCoeffList.add(4);
		secondPolynomialCoeffList.add(3);
		Polynomial secondPolynomial = new Polynomial();
		secondPolynomial.createPolynomial(2, secondPolynomialCoeffList);

		Polynomial resultPolynomial = firstPolynomial.addPolynomial(secondPolynomial);

		assertEquals("4x^3 + 5x^2 + 7x + 6", resultPolynomial.toString());
	}

	@Test
	public void testAddPolynomial_secondPolynomialHigherDegree() {
		ArrayList<Integer> firstPolynomialCoeffList = new ArrayList<Integer>();
		firstPolynomialCoeffList.add(4);
		firstPolynomialCoeffList.add(3);
		firstPolynomialCoeffList.add(2);
		Polynomial firstPolynomial = new Polynomial();
		firstPolynomial.createPolynomial(2, firstPolynomialCoeffList);

		ArrayList<Integer> secondPolynomialCoeffList = new ArrayList<Integer>();
		secondPolynomialCoeffList.add(2);
		secondPolynomialCoeffList.add(4);
		secondPolynomialCoeffList.add(3);
		secondPolynomialCoeffList.add(6);
		Polynomial secondPolynomial = new Polynomial();
		secondPolynomial.createPolynomial(3, secondPolynomialCoeffList);

		Polynomial resultPolynomial = firstPolynomial.addPolynomial(secondPolynomial);

		assertEquals("6x^3 + 5x^2 + 7x + 6", resultPolynomial.toString());
	}
}
